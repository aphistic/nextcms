﻿// -----------------------------------------------------------------------
// <copyright file="NextCms.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using NextCms.Shared.Content;

namespace NextCms.Shared.Database
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Data.Entity;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class NextCmsContext : DbContext
    {
        public DbSet<Page> Pages { get; set; }
        public DbSet<PageContent> Contents { get; set; }

        private NextCmsContext() : base("ConnectionString")
        {
            
        }

        public static NextCmsContext Create()
        {
            return new NextCmsContext();
        }
    }
}
