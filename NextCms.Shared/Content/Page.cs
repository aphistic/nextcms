﻿// -----------------------------------------------------------------------
// <copyright file="Page.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;

namespace NextCms.Shared.Content
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class Page
    {
        public int PageId { get; set; }

        [Required]
        [StringLength(250, MinimumLength = 5)]
        public string Title { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Slug { get; set; }

        public virtual Page Parent { get; set; }
        public virtual List<Page> Children { get; set; }

        public virtual List<PageContent> Contents { get; set; } 

        public Page()
        {
            Children = new List<Page>();
            Contents = new List<PageContent>();
        }
    }
}
