﻿// -----------------------------------------------------------------------
// <copyright file="PageContent.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;

namespace NextCms.Shared.Content
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class PageContent
    {
        public int PageContentId { get; set; }

        public virtual Page Parent { get; set; }

        public string Content { get; set; }
    }
}
