﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NextCms.Shared.Content;
using NextCms.Shared.Database;

namespace NextCms.Areas.Admin.Controllers
{
    public class PageController : Controller
    {
        //
        // GET: /Admin/Page/
        public ActionResult Index()
        {
            using (var db = NextCmsContext.Create())
            {
                return View(db.Pages.ToList());
            }
        }

        public ActionResult Create()
        {
            return View(new Page());
        }

        [HttpPost]
        public ActionResult Create(Page model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            using (var db = NextCmsContext.Create())
            {
                model.Contents.Add(new PageContent {Content = "Test Content 1"});
                model.Contents.Add(new PageContent {Content = "Test Content 2"});

                int parentId;
                if (int.TryParse(Request.Form["ParentPageId"], out parentId))
                {
                    model.Parent = (from p in db.Pages
                                    where p.PageId == parentId
                                    select p).FirstOrDefault();
                }

                db.Pages.Add(model);

                db.SaveChanges();
            }
            return RedirectToAction("Edit", "Page", new {id = model.PageId});
        }

        public ActionResult Edit(int id)
        {
            using (var db = NextCmsContext.Create())
            {
                var model = (from p in db.Pages
                             where p.PageId == id
                             select p).FirstOrDefault();

                if (model == null)
                {
                    throw new HttpException(404, "NotFound");
                }

                ViewBag.BlankPage = new Page();
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(Page model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (var db = NextCmsContext.Create())
            {
                model = (from p in db.Pages
                         where p.PageId == model.PageId
                         select p).FirstOrDefault();
                TryUpdateModel(model);

                db.SaveChanges();
            }

            return View(model);
        }
    }
}
